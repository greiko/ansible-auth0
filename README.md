# ansible-auth0


## Applications 

If application with specified name does not exist, it will create it. If exist, then it will update it.

**Auth0 can have two application with the same name** 

See variables.yml for example

## Resource Servers (APIS) 

If a resource server  with specified name does not exist, it will create it. If exist, then it will update it.

**Auth0 can have two resource servers with the same name** 

See variables.yml for example